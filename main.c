
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void titulo()
{
	printf(" ██████╗██╗  ██╗██████╗  ██████╗ ███╗   ██╗ ██████╗ ███████╗\n");
 printf("██╔════╝██║  ██║██╔══██╗██╔═══██╗████╗  ██║██╔═══██╗██╔════╝\n");
 printf("██║     ███████║██████╔╝██║   ██║██╔██╗ ██║██║   ██║███████╗\n");
 printf("██║     ██╔══██║██╔══██╗██║   ██║██║╚██╗██║██║   ██║╚════██║\n");
 printf("╚██████╗██║  ██║██║  ██║╚██████╔╝██║ ╚████║╚██████╔╝███████║\n");
  printf(" ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝ ╚══════╝\n");

}

void start()
{
	int S = 0; //segundos
  int M = 0; //minutos
  int H = 0; //horas
  int i; //contador para o for
	char Enter;

	titulo();

	printf("\n\t|Pressione Enter para iniciar a contagem|\n");
	scanf("%c", &Enter);

	for(i = 0; i < 5; --i)
  {
	 /* Exibe o tempo, que é atualizado a cada 1 segundo junto da função "sleep".*/
		 printf("\n\t\t\t%d h : %d min : %d seg", H, M, S);

		 /*
		 O sleep tem função de deixar o programa em modo de espera por um tempo definido.
		 Assim, a proxima ação é só executada depois do tempo em segundos informado na função "sleep"
		 Requer o uso da biblioteca "<unistd.h> em UNIX-Based Systems".
		 */
		 sleep(1); /*O parâmetro "1" equivale a 1 segundo.*/

		 /* Tem a função de limpar a tela no terminal, requer o uso da biblioteca "<stdlib.h>".*/
		 system("clear");
		 S++;

		 /*
		 Quando a variável do segundo atinge valor 60, a variável
		 do minuto é incrementada, e segundo passa a assumir valor 0, assim reiniciando a contagem dos segundos.
		 */
		 if (S == 60)
		 {
				 S = 0;
				 M++;
		 }

				/*
				Similar a estrutura acima...
				Quando a variável "minuto" atinge valor 60, a variável
				"hora" é incrementada, e minuto passa a assumir valor 0, assim reiniciando a contagem.
				*/
				if (M == 60)
				{
						M = 0;
						H++;
				}
  }
}

int main(int argc, char *argv[])
{

		if(argc != 2)
		{
			printf("Erro: Nenhuma operação declarada. (Use -h para ajuda)\n");
			return 1;
		}

    //Se digitado ./main -h, sera imprimido um menu de ajuda
		if(strcmp (argv[1], "-h") == 0)
		{
			printf("./main -S = \"Start\", modo de contagem normal.\n");
		}

		//Se digitado ./main -S, o modo de contagem normal sera iniciado
		if(strcmp(argv[1], "-S") == 0)
		{
      start();
		}

return 0;
}
